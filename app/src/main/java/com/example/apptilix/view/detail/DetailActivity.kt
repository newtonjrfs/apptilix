package com.example.apptilix.view.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.apptilix.R
import com.example.apptilix.model.Data
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val itemArticles: Data by lazy { intent.getParcelableExtra<Data>("item") }

        detailTitle.setText("Titulo : " + itemArticles.title)
        detailContent.setText("Content : " + itemArticles.content)
        detailAuthors.setText("Authors : " + itemArticles.authors)
        detailDate.setText("Data : " + itemArticles.date)
        detailTagsId.setText("Id Tags : " + itemArticles.tags[0].id)
        detailTagLabel.setText("Label Tags : " + itemArticles.tags[0].label)

        Picasso.get().load(itemArticles.image_url).into(detailImageView)
        //Picasso.get().load("https://www.bargasfilho.com.br/wp-content/uploads/2018/03/pinga.jpg").into(detailImageView)


    }
}
