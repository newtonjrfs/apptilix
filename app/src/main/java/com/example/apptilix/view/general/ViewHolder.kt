package com.example.apptilix.view.general

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.apptilix.R
import com.example.apptilix.model.Data
import com.example.apptilix.view.detail.DetailActivity
import com.squareup.picasso.Picasso

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val recTextTitle by lazy { itemView.findViewById(R.id.textTitle) as TextView }
    private val recTextWebsite by lazy { itemView.findViewById(R.id.textWebsite) as TextView }
    private val recTextDate by lazy { itemView.findViewById(R.id.textDate) as TextView }
    private val recTextAuthor by lazy { itemView.findViewById(R.id.textAuthor) as TextView }
    private val recTextImage by lazy { itemView.findViewById(R.id.imageTexts) as ImageView }

    fun popular(texts: Data) {

        itemView.setOnClickListener {
            val intent =
                Intent(itemView.context , DetailActivity::class.java)
            val objectText = texts
            intent.putExtra("item" , objectText)
            itemView.context.startActivity(intent)
        }



        recTextTitle.text = "Titulo: " + texts.title
        recTextWebsite.text = "Website: " + texts.website
        recTextDate.text = "Data: " + texts.date
        recTextAuthor.text = "Author: " + texts.authors

        /* link utilizado para carregar uma imagem existente
            texts.image_url = "https://www.bargasfilho.com.br/wp-content/uploads/2018/03/pinga.jpg"
         */
        Picasso.get().load(texts.image_url)
            .into(recTextImage)


    }

}