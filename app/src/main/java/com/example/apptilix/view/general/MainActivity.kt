package com.example.apptilix.view.general

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apptilix.R
import com.example.apptilix.model.Articles
import com.example.apptilix.model.Data
import com.example.apptilix.presenter.Contract
import com.example.apptilix.presenter.Presenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , Contract.View {

    val presenter by lazy { Presenter(this@MainActivity) }


    override fun searchList(result: Articles) {

        val articles by lazy { ArrayList<Data>() }


        articles.addAll(result.data)

        val adapter = MainAdapter(articles)

        recyclerResults.adapter = adapter


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerResults = findViewById<RecyclerView>(R.id.recyclerResults)
        recyclerResults.layoutManager = LinearLayoutManager(this , LinearLayoutManager.VERTICAL , false)

        buttonRequest.setOnClickListener {

            presenter.displayData()
        }


    }
}
