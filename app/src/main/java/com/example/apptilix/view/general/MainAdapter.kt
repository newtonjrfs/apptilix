package com.example.apptilix.view.general

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apptilix.R
import com.example.apptilix.model.Data

class MainAdapter(val listTexts: ArrayList<Data>) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_texts , parent , false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return listTexts.size
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        val texts: Data = listTexts[position]
        holder.popular(texts)

    }


}