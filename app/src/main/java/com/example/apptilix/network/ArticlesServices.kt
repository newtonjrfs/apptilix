package com.example.apptilix.network

import com.example.apptilix.model.Articles
import retrofit2.Call
import retrofit2.http.GET


interface ArticlesServices {

    @GET("articles.json")
    fun searchArticles(): Call<Articles>


}