package com.example.apptilix.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {


    private val retrofit = Retrofit.Builder()
        .baseUrl("https://bitbucket.org/!api/2.0/snippets/tilix-dev/AeLkqo/57748e55ada68faa003a283872e32d098c837f0c/files/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()


    fun searchArticle() = retrofit.create(ArticlesServices::class.java)


}