package com.example.apptilix.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tag(
    val id: Int ,
    val label: String
) : Parcelable