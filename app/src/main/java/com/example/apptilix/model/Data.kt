package com.example.apptilix.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    val authors: String ,
    val content: String ,
    val date: String ,
    val image_url: String ,
    val tags: List<Tag> ,
    val title: String ,
    val website: String
) : Parcelable {
}