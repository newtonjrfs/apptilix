package com.example.apptilix.presenter

import android.util.Log
import com.example.apptilix.model.Articles
import com.example.apptilix.network.RetrofitInitializer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Interector {

    fun searchData(callback: (Articles) -> Unit) {
        val call = RetrofitInitializer().searchArticle().searchArticles()


        call.enqueue(object : Callback<Articles?> {
            override fun onResponse(
                call: Call<Articles?> ,
                response: Response<Articles?>
            ) {

                response.body()?.let {
                    callback.invoke(it)

                }

            }

            override fun onFailure(call: Call<Articles?>? , t: Throwable?) {
                Log.e("onFailure error" , t?.message)
            }
        })

    }
}
