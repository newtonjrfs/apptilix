package com.example.apptilix.presenter

import com.example.apptilix.model.Articles

interface Contract {

    interface Presenter {
        fun displayData()
    }

    interface View {
        fun searchList(result: Articles)

    }
}