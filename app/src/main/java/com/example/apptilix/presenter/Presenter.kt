package com.example.apptilix.presenter

class Presenter(val view: Contract.View) : Contract.Presenter {

    val interector by lazy { Interector() }


    override fun displayData() {
        interector.searchData { result -> view.searchList(result) }
    }


}